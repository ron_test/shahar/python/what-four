number = int(input("enter 4-digits number\n"))

while len(str(number)) != 4:
    number = int(input("enter 4-digits number\n"))

print("the number is", number)

number_as_string = str(number)
first_digit = number_as_string[0]
second_digit = number_as_string[1]
third_digit = number_as_string[2]
forth_digit = number_as_string[3]

print("the first digit is", first_digit)
print("the second digit is", second_digit)
print("the third digit is", third_digit)
print("the forth digit is", forth_digit)

sum_of_digits = int(first_digit) + int(second_digit) + \
    int(third_digit) + int(forth_digit)

print("the sum of digits is", sum_of_digits)
